<%@page import="com.motionpaytech.pay.bean.CallbackServlet"%>
<%
	String orderId = request.getParameter("orderId");
	String paymentAmount = request.getParameter("paymentAmount");
	String paymentType = request.getParameter("paymentType");
	
	if(orderId == null || orderId.length() == 0) {
		out.print("orderId cannot be empty");
	}
	else if(paymentAmount == null || paymentAmount.length() == 0) {
		out.print("paymentAmount cannot be empty");
	}
	else if(paymentType == null || paymentType.length() == 0) {
		out.print("paymentType cannot be empty");
	}	
	else {
			String resultStr = "failed";
			// use query to check if the order has been paid.
			String outTradeNo = new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date()) + orderId;
			
			com.motionpaytech.pay.bean.PrePayBean bean = new com.motionpaytech.pay.bean.PrePayBean();
			java.util.Hashtable<String, String> result = null;
			if(paymentType != null && (paymentType.compareTo("U") == 0 || paymentType.compareTo("QR_PA") == 0 || paymentType.compareTo("QR_PW") == 0)) {
				result = bean.cancelOrderOfflinePos(outTradeNo);
				String code = result.get("code");
				if(code.compareTo("0") == 0) {
					String state = result.get("state");
					if(state != null && state.compareToIgnoreCase("5") == 0) {
						resultStr = "cancelled";
					}
					
				}
			}
			out.print(resultStr);
	}	
%>
