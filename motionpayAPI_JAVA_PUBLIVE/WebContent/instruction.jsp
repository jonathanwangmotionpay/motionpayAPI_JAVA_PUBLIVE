<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Instruction</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
</script>	

</head>
<body>

<!-- header -->
<div id="header">
<div class="logo">
  <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
</div>
<a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
  <div class="infor_box" style="height:30px;">
    <span>Sample Payment Project Instructions by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
  </div>
</div>

<div class="pay_infor" style="text-align: left;">
<pre>
Please download the Eclipse project file from this <a href="motionpayAPI_JAVA_PUB.zip"><u>link</u></a>. 
Please download the develop tool <a href='http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/oxygenm2'>Eclipse IDE for Java EE Developers Oxygen Packages</a> 
we are using.
The project can be build with JDK 1.6, 1.7 and 1.8.
Please download <a href='motionpayAPI_JAVA_PUB.war'>the war file</a> we have generated if you want to deploy to Tomcat directly.
The tomcat folder we are using is: /home/tomcat/tomcat7. You can use another folder, 
the code will read environment variable CATALINA_HOME to set the working folder。
Please download <a href='https://tomcat.apache.org/download-70.cgi'>the Tomcat server 7.0</a> if you don't have it. 

1. You can try to run the main function in the com.motionpay.pay.bean.test.PrePayTest class 
   to generate a QR file. 
   If the QR file was generated successfully, you can try to scan it by your mobile APP to 
   pay it. 
   
2. All the configuration is in the com.motionpay.onlineapi.db.AppConfigInitUtil class. 
   It is the demo URL and demo account for testing for now. After you register and finish 
   the integration, you can replace it with the live URL and live account.
   
3. We are using tomcat 7 for testing. You can run it in tomcat 8 too. You just need to export
   it as a war file and upload it to the tomcat webapp folder to run it.
   Visit index.jsp to select the payment method. We can support 3 types of payment now:
   1) PC WEB browser generate a QR code on the screen then use mobile APP AliPay to 
   scan it for the payment.
   2) PC WEB browser generate a QR code on the screen then use mobile APP Wechat Pay 
   to scan it for the payment.
   3) Mobile browser to make the payment directly by H5 in AliPay.
   
4. If you have any question or need any help, please send an email to 
   jonathan.wang@motionpay.ca.
</pre> 

<br/> <br/>
<pre>
请点击该链接下载 API demo的 Eclipse 工程文件包： <a href="motionpayAPI_JAVA_PUB.zip"><u>下载</u></a>. 
请点击该链接下载我们正在使用的开发工具  <a href='http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/oxygenm2'>Eclipse Java EE Developers Oxygen</a>.
请点击该链接下载我们生成的 <a href='motionpayAPI_JAVA_PUB.war'>the war file</a>， 可以直接部署到 tomcat 7服务器中，
我们使用的tomcat目录是 /home/tomcat/tomcat7. 您可以部署在其他目录下，程序会读取
环境变量 CATALINA_HOME 来设置工作目录。
请点击该链接下载 tomcat 7 服务器软件  <a href='https://tomcat.apache.org/download-70.cgi'>the Tomcat server 7.0</a>. 

1. 你可以直接运行com.motionpay.pay.bean.test.PrePayTest类里面的 main 函数来进行测试。
          测试过程中会生成一个 二维码 图形文件 在 C:\temp\ 目录下。你可以通过手机里的 
          微信支付或是支付宝来扫码付款。 
   
2. 所以的配置信息都在com.motionpay.onlineapi.db.AppConfigInitUtil类文件中。
          目前该文件中的信息是测试URL和测试账户。您完成集成工作并申请获得正式账户之后，
          可以替换掉该文件中的参数。
           
3. 该工程文件编译生成的 war文件部署在tomcat7 webapps中，进行测试。
          您也可以在 tomcat8中运行。
          您可以访问 index.jsp文件来进行测试。我们目前支持三种集成方式：
   1) 在个人电脑的浏览器中生成一个二维码，然后使用支付宝进行扫码支付。
   2) 在个人电脑的浏览器中生成一个二维码，然后使用微信支付进行扫码支付。
   3) 在手机浏览器中，使用H5跳转到支付宝 应用里面直接进行支付.
   
4. 如果您有任何技术问题或是需要帮助，请发送邮件到 jonathan.wang@motionpay.ca 多谢！
</pre>
</div>
</div>
</body>
</html>