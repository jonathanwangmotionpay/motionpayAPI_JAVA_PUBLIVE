<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
</script>	
<style>
table, td, th {
    text-align: left;
}
</style>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span>Sample Order Detail Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
      <span style="float:right;"><a href="orderList.jsp"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a></span> 
    </div>
</div>
  
  <form id="payment" action="orderRefund.jsp" method="post" >
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Order Detail Sample Page</label></h6>
  	  <ul class="sel_list">
<%
			com.motionpaytech.pay.bean.PrePayBean bean = new com.motionpaytech.pay.bean.PrePayBean();
			String outTradeNo = request.getParameter("OutTradeNo");
			String paymentType = request.getParameter("paymentType");
			
			java.util.Hashtable<String, String> result = bean.queryOrder(outTradeNo,paymentType);
			String code = result.get("code");
			if(code.compareTo("0") == 0) {
				if(paymentType != null && (paymentType.compareToIgnoreCase("U") == 0 || paymentType.compareTo("QR_PA") == 0 || paymentType.compareTo("QR_PW") == 0)) {
					String amountPaidInCents = result.get("amount");
					if(amountPaidInCents == null || amountPaidInCents.length() == 0) {
						amountPaidInCents = "0";
					}
					float amountInFloat = (new Float(amountPaidInCents)).floatValue() / 100;
					String amountPaid = String.format("%.2f", amountInFloat);
					String state = result.get("state"); // Transaction State (1: Paying, 2: Paid, 3: Refund, 4: Closed, 5: Cancelled )
					if(state != null) {
						if(state.compareToIgnoreCase("1") == 0) {
							state = state + " - Paying";
						}
						if(state.compareToIgnoreCase("2") == 0) {
							state = state + " - Paid";
						}
						if(state.compareToIgnoreCase("3") == 0) {
							state = state + " - Refund";
						}
						if(state.compareToIgnoreCase("4") == 0) {
							state = state + " - Closed";
						}
						if(state.compareToIgnoreCase("5") == 0) {
							state = state + " - Cancelled";
						}
					}
					
					out.println("<table>");
					out.println("<tr><td>amount: </td><td>$" + amountPaid + "</td></tr>");
					out.println("<tr><td>merchantOrderNo: </td><td>" + result.get("merchantOrderNo")  + "</td></tr>");
					out.println("<tr><td>tranCode: </td><td>" + result.get("tranCode")  + "</td></tr>");
					out.println("<tr><td>payType:</td><td>" + result.get("payType") + "</td></tr>");
					out.println("<tr><td>state: </td><td>" + state + "</td></tr>");
					out.println("<tr><td>payTime: </td><td>" + result.get("payTime") + "</td></tr>");
					out.println("<tr><td>Refund Amount:  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>");
					out.println("<td><input type='text' name='refundAmount' value='" + amountPaid + "' size='5'/> &nbsp;");
					out.println("<input type='hidden' name='totalAmount' value='" + amountPaid + "'>");
					out.println("<input type='hidden' name='merchantOrderNo' value='" + result.get("merchantOrderNo") + "'>");
					out.println("<input type='hidden' name='paymentType' value='" + paymentType + "'>");
					out.println("<input type='hidden' name='tranCode' value='" + result.get("tranCode") + "'>");
					out.println("<input type='hidden' name='orderNo' value='" + result.get("orderNo") + "'>");
					out.println("<input type='hidden' name='tranLogId' value='" + result.get("tranLogId") + "'>");					
					out.println("<input type='submit' name='refund' value='Refund'></td></tr>");
					out.println("</table>");
				}
				else {
					String amountPaidInCents = result.get("total_fee");
					if(amountPaidInCents == null || amountPaidInCents.length() == 0) {
						amountPaidInCents = "0";
					}
					float amountInFloat = (new Float(amountPaidInCents)).floatValue() / 100;
					String amountPaid = String.format("%.2f", amountInFloat);
					
					out.println("<table>");
					out.println("<tr><td>total_fee: </td><td>$" + amountPaid + "</td></tr>");
					out.println("<tr><td>pay_channel:</td><td>" + result.get("pay_channel") + "</td></tr>");
					out.println("<tr><td>trade_status: </td><td>" + result.get("trade_status") + "</td></tr>");
					out.println("<tr><td>user_identify: </td><td>" + result.get("user_identify") + "</td></tr>");
					out.println("<tr><td>Refund Amount:  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>");
					out.println("<td><input type='text' name='refundAmount' value='" + amountPaid + "' size='5'/> &nbsp;");
					out.println("<input type='hidden' name='totalAmount' value='" + amountPaid + "'>");
					out.println("<input type='hidden' name='OutTradeNo' value='" + outTradeNo + "'>");
					out.println("<input type='hidden' name='paymentType' value='" + paymentType + "'>");
					out.println("<input type='submit' name='refund' value='Refund'></td></tr>");
					out.println("</table>");
					// out.println(result);
				}
			}
			else {
				out.println("The order doesn't exist or has not been paid yet.<br/>");
				out.println("message:" + result.get("message"));
			}
%>           
           
      </ul>
    </div>
  </form>
 
</div>

</body>

</html>


