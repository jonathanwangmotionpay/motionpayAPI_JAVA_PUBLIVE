<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
</script>	
<style>
table, td, th {
    text-align: left;
}
</style>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span>Sample Order Refund Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
      <span style="float:right;"><a href="orderList.jsp"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a></span> 
    </div>
</div>
  
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Order Refund Result</label></h6>
  	  <ul class="sel_list">
<%
			com.motionpaytech.pay.bean.PrePayBean bean = new com.motionpaytech.pay.bean.PrePayBean();
			String outTradeNo = request.getParameter("OutTradeNo");
			String refundAmount = "0";
			String totalAmount = "0";
			refundAmount = request.getParameter("refundAmount");
			totalAmount = request.getParameter("totalAmount");
			String paymentType = request.getParameter("paymentType");
			
			// out.println("paymentType is:"+paymentType);
			java.util.Hashtable<String, String> result = null;
			// if it is POS without server Refund
			if(paymentType != null && (paymentType.compareToIgnoreCase("U") == 0 || paymentType.compareTo("QR_PA") == 0 || paymentType.compareTo("QR_PW") == 0)) {
				// out.println("refundOrderOfflinePos");
				String tranCode = request.getParameter("tranCode");
				String orderNo = request.getParameter("orderNo");
				String tranLogId = request.getParameter("tranLogId");
				result = bean.refundOrderOfflinePos(tranCode,orderNo,tranLogId,refundAmount,totalAmount);
			}
			else {
				result = bean.refundOrder(outTradeNo,refundAmount,totalAmount,paymentType);
			}
			
			String code = result.get("code");
			if(code.compareTo("0") == 0) {
				// if it is POS without server Refund
				if(paymentType != null && (paymentType.compareToIgnoreCase("U") == 0 || paymentType.compareTo("QR_PA") == 0 || paymentType.compareTo("QR_PW") == 0)) {
					String amountRefundInCents = result.get("refundAmount");
					if(amountRefundInCents == null || amountRefundInCents.length() == 0) {
						amountRefundInCents = "0";
					}
					float amountInFloat = (new Float(amountRefundInCents)).floatValue() / 100;
					String amountRefund = String.format("%.2f", amountInFloat);
					
					amountRefundInCents = result.get("tranAmount");
					if(amountRefundInCents == null || amountRefundInCents.length() == 0) {
						amountRefundInCents = "0";
					}
					amountInFloat = (new Float(amountRefundInCents)).floatValue() / 100;
					String total_fee = String.format("%.2f", amountInFloat);			
					
					String tranCode = result.get("tranCode");
					if(tranCode != null && (tranCode.compareToIgnoreCase("820") == 0 || tranCode.compareToIgnoreCase("809") == 0)) {
						tranCode = tranCode + "- Revoke/Refund";
					}
					out.println("<table>");
					out.println("<tr><td>refundAmount: </td><td>$" + amountRefund + "</td></tr>");
					out.println("<tr><td>total_fee: </td><td>$" + total_fee + "</td></tr>");
					out.println("<tr><td>tranDesc:</td><td>" + result.get("tranDesc") + "</td></tr>");
					out.println("<tr><td>tranCode: </td><td>" + tranCode + "</td></tr>");
					out.println("<tr><td>message: </td><td>" + result.get("message") + "</td></tr>");
					out.println("</table>");					
				}
				else {
					String amountRefundInCents = result.get("refund_fee");
					if(amountRefundInCents == null || amountRefundInCents.length() == 0) {
						amountRefundInCents = "0";
					}
					float amountInFloat = (new Float(amountRefundInCents)).floatValue() / 100;
					String amountRefund = String.format("%.2f", amountInFloat);
					
					amountRefundInCents = result.get("total_fee");
					if(amountRefundInCents == null || amountRefundInCents.length() == 0) {
						amountRefundInCents = "0";
					}
					amountInFloat = (new Float(amountRefundInCents)).floatValue() / 100;
					String total_fee = String.format("%.2f", amountInFloat);
					
					out.println("<table>");
					out.println("<tr><td>refund_fee: </td><td>$" + amountRefund + "</td></tr>");
					out.println("<tr><td>total_fee: </td><td>$" + total_fee + "</td></tr>");
					out.println("<tr><td>pay_channel:</td><td>" + result.get("pay_channel") + "</td></tr>");
					out.println("<tr><td>refund_status: </td><td>" + result.get("refund_status") + "</td></tr>");
					out.println("<tr><td>message: </td><td>" + result.get("message") + "</td></tr>");
					out.println("</table>");
				}
			}
			else {
				out.println("Refund Failed. <br/>");
				out.println("message:" + result.get("message"));
			}
%>           
           
      </ul>
    </div>

 
</div>

</body>

</html>


