<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>

	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
	
<%
	com.motionpaytech.pay.bean.PrePayBean bean = new com.motionpaytech.pay.bean.PrePayBean();	

	String serverName = request.getServerName(); 
	int serverPort = request.getServerPort();    
	String url = "https://" +serverName + "/";
	System.out.println("URL is:" + url);
	bean.setReturnHostURL(url);

	String jsp_path = getServletContext().getRealPath("/");
	jsp_path = jsp_path.substring(0,jsp_path.lastIndexOf("/"));
	System.out.println("jsp_path is:" + jsp_path);
	String webappName = jsp_path.substring(jsp_path.lastIndexOf("/")+1,jsp_path.length());
	System.out.println("webappName is:" + webappName);
	bean.setTomcatWebAppName(webappName);
	
	String demoServerURL = url + webappName + "/";
	System.out.println("demoServerURL is:" + demoServerURL);
	
	java.util.Random random = new java.util.Random();
	long id = random.nextInt(99999999);
	
 	String errorMsg = "";
	String paymentType = "";
	String orderId = String.format("%08d", id);
	String terminalNo = "Webserver1";
	String paymentAmount = "1";
	String amountInReq = request.getParameter("paymentAmount");
	if(amountInReq != null && amountInReq.length() > 0) {
		try {
			float amountInFloat = (new Float(amountInReq)).floatValue() * 100;
			int valueInt = (int) amountInFloat;
			paymentAmount = (new Integer(valueInt)).toString();
			System.out.println("paymentAmount is:"+paymentAmount);
		}
		catch (Exception e) {}
	}
	String productName = "Third Party Product";
	String filename = "";
	
 	String type = request.getParameter("paymentInfo.bankSelect"); 
 	if(type == null)
 		type = "";
 	String scanImageHtmlCode = "";
	paymentType = type;

 	if(type.compareTo("W") == 0) {
 		scanImageHtmlCode = "<img src='images/wechatpay.png'/>";
 	} else if(type.compareTo("A") == 0) {
 		scanImageHtmlCode = "<img src='images/alipay.png'/>";
 	} else if(type.compareTo("H5_A") == 0) {
 		scanImageHtmlCode = "<img src='images/alipayH5.png'/>";
 	} else if(type.compareTo("H5_W") == 0) {
 		scanImageHtmlCode = "<img src='images/wechatH5.png'/>";
	} else if(type.compareTo("SM_A") == 0) {
 		scanImageHtmlCode = "<img src='images/scanMobileA.png'/>";
	} else if(type.compareTo("SM_W") == 0) {
 		scanImageHtmlCode = "<img src='images/scanMobileW.png'/>";
 	} else if(type.compareTo("SM_PU") == 0) {
 		scanImageHtmlCode = "<img src='images/scanMobilePU.png'/>";
 	} else if(type.compareTo("QR_PA") == 0) {
 		scanImageHtmlCode = "<img src='images/alipayQR_PA.png'/>";
 	} else if(type.compareTo("QR_PW") == 0) {
 		scanImageHtmlCode = "<img src='images/wechatQR_PW.png'/>";
 	}
 	else {
 		errorMsg = "We can only support Wechat and Alipay for now.";
 	}	
%>	 

<script>
var myTimeoutVar;
var timeoutCount = 5000;    // online query is every 5 seconds
var countDownTimer = 300;	// 300 seconds to pay

<%
if(paymentType != null && (type.compareTo("QR_PA") == 0 || type.compareTo("QR_PW") == 0) ) {
%>	
	timeoutCount = 30000;   // POS query is every 30 seconds
	setTimeout(showCountDownCancelDiv, 500);
<%	
}
%>

function updateCountDownClock() {
	// alert("updateCountDownClock");
	var divCountDown = document.getElementById("countDown");
	if(divCountDown != null) {
		// alert("count down seconds!");
		divCountDown.innerHTML = countDownTimer + "";
		countDownTimer = countDownTimer - 1;
		if(countDownTimer >= 0) {
			setTimeout(updateCountDownClock, 1000);
		}
		else {
			cancelOrder();
		}
	}
}

function showCountDownCancelDiv() {
	// alert("showCountDownCancelDiv");
	var divForCancel = document.getElementById("countDownCancelDiv");
	if(divForCancel != null) {
		// alert("show the div.");
		divForCancel.style.display = "block";
		updateCountDownClock();
	}
	else {
		// alert("div is null.");
	}
}

function orderHasBeenCancelled() {
	var divForCancel = document.getElementById("countDownCancelDiv");
	if(divForCancel != null) {
		// alert("show the div.");
		divForCancel.style.display = "none";
	}
	var divToUpdate = document.getElementById("infor_box");
	divToUpdate.innerHTML = "<br/><font class='cOrange' style='font-size: 25px;'>The order has been cancelled.</font>";	
	clearTimeout(myTimeoutVar);
}

function checkOrder() {
	// alert("checkOrder");
	var checkResult = checkPaymentResult();
	if(checkResult == false) {
		var divOrderStatus = document.getElementById("orderStatus");
		if(divOrderStatus != null) {
			divOrderStatus.innerHTML = "The order is not paid yet. Checked when " + countDownTimer + " seconds are left.";
		}
	}
}

function cancelOrder() {
	// alert("cancelOrder function!");
	var result = false;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "cancelOrder.jsp?orderId=<%= orderId %>&paymentAmount=<%= paymentAmount %>&paymentType=<%= paymentType %>&", true);
	xhr.onload = function (e) {
	  if (xhr.readyState === 4) {
	    if (xhr.status === 200) {
	      var resultStr = xhr.responseText;
	      console.log(resultStr);
	      resultStr = resultStr.trim();
	      // alert("cancelOrder is:#" + resultStr +"#");
	      if(resultStr == "cancelled") {
	    	  result = true;
	    	  orderHasBeenCancelled();
	      }
	    } else {
	      console.error("status error:" + xhr.statusText);
	    }
	  }
	};
	xhr.onerror = function (e) {
	  console.error("function error:" + xhr.statusText);
	};
	if(result == false) {
		xhr.send(null);
	}
	return result;
}

function checkPaymentResult() {
	var paid = false;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "paymentNotify.jsp?orderId=<%= orderId %>&paymentAmount=<%= paymentAmount %>&paymentType=<%= paymentType %>&", true);
	xhr.onload = function (e) {
	  if (xhr.readyState === 4) {
	    if (xhr.status === 200) {
	      var resultStr = xhr.responseText;
	      console.log(resultStr);
	      resultStr = resultStr.trim();
	      // alert("resultStr is:#" + resultStr +"#");
	      if(resultStr == "paid") {
	    	  // alert("payment is done.");
	    	  paid = true;
	    	  var divToUpdate = document.getElementById("infor_box");
	    	  divToUpdate.innerHTML = "<br/><font class='cOrange' style='font-size: 25px;'>Thank you very much for your payment.</font>";
	    	  clearTimeout(myTimeoutVar);
	      }
	    } else {
	      console.error(xhr.statusText);
	    }
	  }
	};
	xhr.onerror = function (e) {
	  console.error(xhr.statusText);
	};
	if(paid == false) {
		xhr.send(null);
		myTimeoutVar = setTimeout(checkPaymentResult, timeoutCount);
	}
	return paid;
}

function setTimeoutCheckFunc() {
	myTimeoutVar = setTimeout(checkPaymentResult, timeoutCount);
}

<%
if(type.compareTo("W") == 0 || type.compareTo("A") == 0 || type.compareTo("QR_PW") == 0 || type.compareTo("QR_PA") == 0) {
%>
	setTimeoutCheckFunc();
<%
}


%>

</script>	
  </head>
 <body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="#" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
    <div class="pay_infor"  >
    <p><font class="cOrange" style="font-size: 25px;">Order id: <%= orderId %></font></p>
    <br/>
    <div id="infor_box" class="infor_box" style="height:430px;">
 
 <%
 	if(errorMsg.length() == 0) {
 		
 		String callbackURL = demoServerURL + "CallbackServlet";
 		
 		if(type.compareTo("H5_A") == 0) {
 			session.setAttribute("orderId", orderId);
 			session.setAttribute("paymentAmount", paymentAmount);
 			// we cannot have & in the URL as aplipay cannot accept it.
 			// String wap_URL = demoServerURL +  "paymentDone.jsp?orderId=" + orderId + "&paymentAmount=" + paymentAmount;
 			String wap_URL = demoServerURL +  "paymentDone.jsp?orderId=" + orderId;
 			System.out.println("wap_URL is:" + wap_URL);
 			String pay_URL = bean.generateH5URL(paymentType, orderId, terminalNo, paymentAmount, productName, callbackURL, wap_URL); 
 			System.out.println("pay_URL is:" + pay_URL);
%>
 			<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
 			Please make the payment in AliPay.</font></p>
<%			
			if(pay_URL != null && pay_URL.length() > 0) {
				response.sendRedirect(pay_URL);
			}
 		}
 		else if(type.compareTo("H5_W") == 0) {
 			session.setAttribute("orderId", orderId);
 			session.setAttribute("paymentAmount", paymentAmount);
 			// String wap_URL = demoServerURL +  "paymentDone.jsp?orderId=" + orderId + "&paymentAmount=" + paymentAmount;
 			String wap_URL = demoServerURL +  "paymentDone.jsp?orderId=" + orderId;
 			// String wap_URL = demoServerURL +  "paymentDone.jsp";
 			String pay_URL = bean.generateH5URL(paymentType, orderId, terminalNo, paymentAmount, productName, callbackURL, wap_URL); 
 			System.out.println("pay_URL is:" + pay_URL);
%>
 			<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
 			Please make the payment in WeChat.</font></p>
<%			
			if(pay_URL != null && pay_URL.length() > 0) {
				response.sendRedirect(pay_URL);
			}
 		}
 		else if(type.compareTo("SM_A") == 0) {
 			session.setAttribute("orderId", orderId);
 			session.setAttribute("paymentAmount", paymentAmount);
%>
	 		<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
	 		Please input the bar code number from you cell phone to pay it:</font></p>
	 		<br/>
	 		<img src="images/scanMobileA.png"/><br/>
	 		<form id="payment" action="paymentDone.jsp" method="post" >
	 		<input type="hidden" name="orderId" value="<%= orderId %>"/>
	 		<input type="hidden" name="paymentAmount" value="<%= paymentAmount %>"/>
	 		<input type="hidden" name="paymentType" value="<%= paymentType %>"/>
	 		<input type="text" name="barcode" value=""/>
	 		<input type="submit" name="pay" value="pay"/>
	 		</form>
	 		
<%			
 		}
 		else if(type.compareTo("SM_W") == 0) {
 			session.setAttribute("orderId", orderId);
 			session.setAttribute("paymentAmount", paymentAmount);
%>
	 		<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
	 		Please input the bar code number from you cell phone to pay it:</font></p>
	 		<img src="images/scanMobileW.png"/><br/>
	 		<form id="payment" action="paymentDone.jsp" method="post" >
	 		<input type="hidden" name="orderId" value="<%= orderId %>"/>
	 		<input type="hidden" name="paymentAmount" value="<%= paymentAmount %>"/>
	 		<input type="hidden" name="paymentType" value="<%= paymentType %>"/>
	 		<input type="text" name="barcode" value=""/>
	 		<input type="submit" name="pay" value="pay"/>
	 		</form>
<% 			
 		}
 		else if(type.compareTo("SM_PU") == 0) {
 			session.setAttribute("orderId", orderId);
 			session.setAttribute("paymentAmount", paymentAmount);
%>
	 		<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
	 		Please input the bar code number from you cell phone to pay it:</font></p>
	 		<img src="images/scanMobilePU.png"/><br/>
	 		<form id="payment" action="paymentDone.jsp" method="post" >
	 		<input type="hidden" name="orderId" value="<%= orderId %>"/>
	 		<input type="hidden" name="paymentAmount" value="<%= paymentAmount %>"/>
	 		<input type="hidden" name="paymentType" value="<%= paymentType %>"/>
	 		<input type="text" name="barcode" value=""/>
	 		<input type="submit" name="pay" value="pay"/>
	 		</form>
<% 			
 		}
 		else if(type.compareTo("QR_PA") == 0 || type.compareTo("QR_PW") == 0) {
	 		filename = bean.generateQRImageOfflinePOS(paymentType, orderId, terminalNo, paymentAmount, productName);
%>
	 		<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
	 		Please scan this image from you cell phone to pay it:</font></p>
	 		 <br/>
	 		 <%= scanImageHtmlCode %> <br/>
	 			 <img src="images/<%=filename%>"/>
	 			 
	 		<div id="countDownCancelDiv" style="display:none;">
	 		
	 			 
	 			<font class="cOrange" style="font-size: 20px;"><span id="countDown" style="font-size: 25px;">300</span> seconds left for payment. </font>
	 			<br/><font class="cOrange" id="orderStatus" style="font-size: 15px;">After that the order will be cancelled automatically.</font>
	 			
	 		<br/><br/>

		 			<button class="greenbutton" onclick="checkOrder();">Check Order</button>
		 			<button class="button" onclick="cancelOrder();">Cancel Order</button>

	 		</div>
	 			 			 
<%			 
 		}
 		else {
	 		filename = bean.generateQRImageOnline(paymentType, orderId, terminalNo, paymentAmount, productName, callbackURL);
%>
	 		<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
	 		Please scan this image from you cell phone to pay it:</font></p>
	 		 <br/>
	 		 <%= scanImageHtmlCode %> <br/>
	 			 <img src="images/<%=filename%>"/>
<%			 
 		}
 	}
 	else {
		out.print(errorMsg);
 	}	
%>
 
    </div>
  </div>
  
 
  </div>

</body>
</html>


