<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>

	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>

<%
	String waitingMsg = "Please wait for the payment verification.";

	String orderId = request.getParameter("orderId");
	String paymentAmount = request.getParameter("paymentAmount");
	if(paymentAmount == null || paymentAmount.length() == 0) {
		paymentAmount = (String) session.getAttribute("paymentAmount");
	}
	if(orderId == null || orderId.length() == 0) {
		orderId = (String) session.getAttribute("orderId");
	}

	String paymentType = request.getParameter("paymentType");
	String barcode = request.getParameter("barcode");
	if(paymentType == null || paymentType.length() == 0) {
		paymentType = "H5_A";
	}
	if(paymentType != null && (paymentType.compareTo("SM_A") == 0 || paymentType.compareTo("SM_W") == 0 || paymentType.compareTo("SM_PU") == 0)) {
		if(paymentType.compareTo("SM_A") == 0) {
			paymentType = "A";
		}
		else if(paymentType.compareTo("SM_W") == 0) {
			paymentType = "W";
		}
		else if(paymentType.compareTo("SM_PU") == 0) {
			paymentType = "U";
		}
		com.motionpaytech.pay.bean.PrePayBean bean = new com.motionpaytech.pay.bean.PrePayBean();	

		String serverName = request.getServerName(); 
		int serverPort = request.getServerPort();    
		String url = "https://" +serverName + "/";
		System.out.println("URL is:" + url);
		bean.setReturnHostURL(url);

		String jsp_path = getServletContext().getRealPath("/");
		jsp_path = jsp_path.substring(0,jsp_path.lastIndexOf("/"));
		System.out.println("jsp_path is:" + jsp_path);
		String webappName = jsp_path.substring(jsp_path.lastIndexOf("/")+1,jsp_path.length());
		System.out.println("webappName is:" + webappName);
		bean.setTomcatWebAppName(webappName);
		
		String demoServerURL = url + webappName + "/";
		System.out.println("demoServerURL is:" + demoServerURL);
		
	 	String errorMsg = "";
		String terminalNo = "Webserver1";
		String productName = "Test Product";		
		String callbackURL = demoServerURL + "CallbackServlet";
		
		String result = "";
		System.out.println("payment type is:" + paymentType);
		if(paymentType.compareTo("U") == 0) {
			System.out.println("Call scanMobilePaymentPOS.");
			result = bean.scanMobilePaymentPOS(barcode, paymentType, orderId, terminalNo, paymentAmount, productName);
			System.out.println("Done Call scanMobilePaymentPOS.");
		}
		else {
			result = bean.scanMobilePayment(barcode, paymentType, orderId, terminalNo, paymentAmount, productName, callbackURL);
		}
		if(result != null && result.length() > 0) {
			waitingMsg = result;
		}
	}
%>	
<script>
var myTimeoutVar;
var timeoutCount = 5000;    // online query is every 5 seconds
var countDownTimer = 300;	// 300 seconds to pay

<%
if(paymentType != null && (paymentType.compareTo("U") == 0) ) {
%>	
	timeoutCount = 30000;   // POS query is every 30 seconds
	setTimeout(showCountDownCancelDiv, 500);
<%	
}
%>

function showCountDownCancelDiv() {
	var divForCancel = document.getElementById("countDownCancelDiv");
	if(divForCancel) {
		divForCancel.style.display = "block";
	}
}

function updateCountDownClock() {
	// alert("updateCountDownClock");
	var divCountDown = document.getElementById("countDown");
	if(divCountDown != null) {
		// alert("count down seconds!");
		divCountDown.innerHTML = countDownTimer + "";
		countDownTimer = countDownTimer - 1;
		if(countDownTimer >= 0) {
			setTimeout(updateCountDownClock, 1000);
		}
		else {
			cancelOrder();
		}
	}
}

function showCountDownCancelDiv() {
	// alert("showCountDownCancelDiv");
	var divForCancel = document.getElementById("countDownCancelDiv");
	if(divForCancel != null) {
		// alert("show the div.");
		divForCancel.style.display = "block";
		updateCountDownClock();
	}
	else {
		// alert("div is null.");
	}
}

function orderHasBeenCancelled() {
	var divForCancel = document.getElementById("countDownCancelDiv");
	if(divForCancel != null) {
		// alert("show the div.");
		divForCancel.style.display = "none";
	}
	var divToUpdate = document.getElementById("infor_box");
	divToUpdate.innerHTML = "<br/><font class='cOrange' style='font-size: 25px;'>The order has been cancelled.</font>";	
	clearTimeout(myTimeoutVar);
}

function checkOrder() {
	// alert("checkOrder");
	var checkResult = checkPaymentResult();
	if(checkResult == false) {
		var divOrderStatus = document.getElementById("orderStatus");
		if(divOrderStatus != null) {
			divOrderStatus.innerHTML = "The order is not paid yet. Checked when " + countDownTimer + " seconds are left.";
		}
	}
}

function cancelOrder() {
	// alert("cancelOrder function!");
	var result = false;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "cancelOrder.jsp?orderId=<%= orderId %>&paymentAmount=<%= paymentAmount %>&paymentType=<%= paymentType %>&", true);
	xhr.onload = function (e) {
	  if (xhr.readyState === 4) {
	    if (xhr.status === 200) {
	      var resultStr = xhr.responseText;
	      console.log(resultStr);
	      resultStr = resultStr.trim();
	      // alert("cancelOrder is:#" + resultStr +"#");
	      if(resultStr == "cancelled") {
	    	  result = true;
	    	  orderHasBeenCancelled();
	      }
	    } else {
	      console.error("status error:" + xhr.statusText);
	    }
	  }
	};
	xhr.onerror = function (e) {
	  console.error("function error:" + xhr.statusText);
	};
	if(result == false) {
		xhr.send(null);
	}
	return result;
}

function checkPaymentResult() {
	var paid = false;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "paymentNotify.jsp?orderId=<%= orderId %>&paymentAmount=<%= paymentAmount %>&paymentType=<%= paymentType %>&", true);
	xhr.onload = function (e) {
	  if (xhr.readyState === 4) {
	    if (xhr.status === 200) {
	      var resultStr = xhr.responseText;
	      console.log(resultStr);
	      resultStr = resultStr.trim();
	      // alert("resultStr is:#" + resultStr +"#");
	      if(resultStr == "paid") {
	    	  // alert("payment is done.");
	    	  paid = true;
	    	  var divToUpdate = document.getElementById("infor_box");
	    	  divToUpdate.innerHTML = "<br/><font class='cOrange' style='font-size: 25px;'>Thank you very much for your payment.</font>";
	    	  clearTimeout(myTimeoutVar);
	      }
	    } else {
	      console.error(xhr.statusText);
	    }
	  }
	};
	xhr.onerror = function (e) {
	  console.error(xhr.statusText);
	};
	if(paid == false) {
		xhr.send(null);
		myTimeoutVar = setTimeout(checkPaymentResult, timeoutCount);
	}
	return paid;
}


function setTimeoutCheckFunc() {
	myTimeoutVar = setTimeout(checkPaymentResult, timeoutCount);
}
setTimeoutCheckFunc();
</script>	
	
  </head>
 <body>


<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="#" class="aProblem">Technology Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
    <div class="pay_infor"  >
    <p><font class="cOrange" style="font-size: 25px;">Order id: <%= orderId %></font></p>
    <br/>
    <div id="infor_box" class="infor_box" style="height:430px;">
 
	 		<br/><font class='cOrange' style='font-size: 25px;'>Please wait for the payment verification.</font>
	 		<div id="countDownCancelDiv" style="display:none;">
	 		
	 			 
	 			<font class="cOrange" style="font-size: 20px;"><span id="countDown" style="font-size: 25px;">300</span> seconds left for payment. </font>
	 			<br/><font class="cOrange" id="orderStatus" style="font-size: 15px;">After that the order will be cancelled automatically.</font>
	 			
	 		<br/><br/>

		 			<button class="greenbutton" onclick="checkOrder();">Check Order</button>
		 			<button class="button" onclick="cancelOrder();">Cancel Order</button>

	 		</div>
    </div>
  </div>
  
 
  </div>

</body>
</html>


