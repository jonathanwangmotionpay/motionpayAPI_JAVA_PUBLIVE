<%@page import="com.motionpaytech.pay.bean.CallbackServlet"%>
<%
	boolean found = false;
	String filename = com.motionpaytech.pay.bean.CallbackServlet.getTransactionsFileName();
	String orderId = request.getParameter("orderId");
	String paymentAmount = request.getParameter("paymentAmount");
	String paymentType = request.getParameter("paymentType");
	
	if(orderId == null || orderId.length() == 0) {
		out.print("orderId cannot be empty");
	}
	else if(paymentAmount == null || paymentAmount.length() == 0) {
		out.print("paymentAmount cannot be empty");
	}
	else {
		// if we have call back notify, we can check the call back log to see if we got the notification
		// for the POS without server API. We needn't to check the call back history. You can just ignore the code in try block
		try {
			java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.FileReader(filename));
			String line = reader.readLine();
			while (line != null) {
			    // do something
			    line = reader.readLine();
			    // out.println("line is:"+line + "<br/>");
			    if(line.indexOf("out_trade_no") < 0)
			    	continue;
			    String jsonStr = line.replaceAll("INFO: ", "");
			    // out.println("jsonStr is:"+jsonStr + "<br/>");
			    com.alibaba.fastjson.JSONObject json = com.alibaba.fastjson.JSONObject.parseObject(jsonStr);   
			    String out_trade_no = json.getString("out_trade_no");
			    // out.println("out_trade_no is:"+out_trade_no);
			    if(out_trade_no != null && orderId != null && orderId.length() > 0 && out_trade_no.endsWith(orderId) == true) {
			    	String pay_result = json.getString("pay_result");
			    	String total_fee = json.getString("total_fee");
			    	if(pay_result == null || pay_result.compareToIgnoreCase("SUCCESS") != 0)
			    		break;
			    	if(total_fee == null || total_fee.compareToIgnoreCase(paymentAmount) != 0)
			    		break;
			    	found = true;
			    	break;
			    }
			}
			reader.close();
		}
		catch(Exception e) {}
		if(found == false) {
			String resultForFinding = "no found";
			// use query to check if the order has been paid.
			String outTradeNo = new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date()) + orderId;
			
			com.motionpaytech.pay.bean.PrePayBean bean = new com.motionpaytech.pay.bean.PrePayBean();
			java.util.Hashtable<String, String> result = null;
			if(paymentType != null && (paymentType.compareTo("U") == 0 || paymentType.compareTo("QR_PA") == 0 || paymentType.compareTo("QR_PW") == 0)) {
				result = bean.queryOrderOfflinePOS(outTradeNo);
				String code = result.get("code");
				if(code.compareTo("0") == 0) {
					String amount = result.get("amount");
					String state = result.get("state");
					if(amount != null && amount.compareToIgnoreCase(paymentAmount) == 0
							&& state != null && state.compareToIgnoreCase("2") == 0) {
						resultForFinding = "paid";
					}
					
				}
			}
			else {
				result = bean.queryOrder(outTradeNo, paymentType);
				String code = result.get("code");
				if(code.compareTo("0") == 0) {
					String total_fee = result.get("total_fee");
					String trade_status = result.get("trade_status");
					if(total_fee != null && total_fee.compareToIgnoreCase(paymentAmount) == 0
							&& trade_status != null && trade_status.compareToIgnoreCase("SUCCESS") == 0) {
						resultForFinding = "paid";
					}
					
				}
			}
			out.print(resultForFinding);
		}
		else {
			out.print("paid");
		}
	}	
%>
