package com.motionpaytech.fastjson.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.motionpaytech.fastjson.parser.Feature;
import com.motionpaytech.fastjson.serializer.SerializerFeature;

/**
 * @author jonathan[jonathan.wang@motionpay.ca]
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface JSONType {

    boolean asm() default true;

    String[] orders() default {};
    
    String[] ignores() default {};

    SerializerFeature[] serialzeFeatures() default {};
    Feature[] parseFeatures() default {};
    
    boolean alphabetic() default true;
    
    Class<?> mappingTo() default Void.class;
    
    /**
     * @since 1.2.11 backport to 1.1.52.android
     */
    String typeName() default "";
    
    /**
     * @since 1.2.11 backport to 1.1.52.android
     */
    Class<?>[] seeAlso() default{};
}
