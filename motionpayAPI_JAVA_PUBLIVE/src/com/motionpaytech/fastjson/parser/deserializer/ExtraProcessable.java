package com.motionpaytech.fastjson.parser.deserializer;

/**
 * 
 * @author jonathan[jonathan.wang@motionpay.ca]
 * @since 1.2.9 back port to 1.1.49.android
 */
public interface ExtraProcessable {
    void processExtra(String key, Object value);
}
