package com.motionpaytech.fastjson.parser.deserializer;

import com.motionpaytech.fastjson.parser.deserializer.ParseProcess;

/**
 * 
 * @author jonathan[jonathan.wang@motionpay.ca]
 * @since 1.1.34
 */
public interface ExtraProcessor extends ParseProcess {

    void processExtra(Object object, String key, Object value);
}
