package com.motionpaytech.fastjson.parser.deserializer;

import java.lang.reflect.Type;

import com.motionpaytech.fastjson.parser.deserializer.ParseProcess;

/**
 * @author jonathan[jonathan.wang@motionpay.ca]
 * @since 1.1.34
 */
public interface ExtraTypeProvider extends ParseProcess {

    Type getExtraType(Object object, String key);
}
