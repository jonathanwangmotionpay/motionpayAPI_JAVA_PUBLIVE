package com.motionpaytech.fastjson.parser.deserializer;

import java.lang.reflect.Type;

import com.motionpaytech.fastjson.parser.DefaultJSONParser;

public interface ObjectDeserializer {
    <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName);
}
