package com.motionpaytech.onlineapi.db;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * TODO
 * Created by Song on 2017/11/3.
 */
public final class AppConfigInitUtil
{
	public static AppConfigInitUtil INSTANCE = null;
	
    private static String appIdOnline = "appIdOnline";

    private static String appIdUnderline = "appIdUnderline";
    
    private static String appSecretOnline = "appSecretOnline";

    private static String appSecretUnderline = "appSecretUnderline";

    private static String midOnline = "midOnline";

    private static String midUnderline = "midUnderline";
    
    private static String tomcatFolder = "tomcatFolder";
    
    private static String tomcatWebAppImageFolder = "tomcatWebAppFolder";
    
    private static String tomcatWebAppName = "motionpayAPI_JAVA_PUB";
    
    private static String returnHostURL = "https://demo.motionpay.org/";
    
    private static String defaultReturnURL = returnHostURL + tomcatWebAppName + "/CallbackServlet";
    
    private static String apiHostURL = "https://online.motionpaytech.com/";
    
    private static String apiHostURL_POS = "https://pos.motionpaytech.com/";

    /**
     * offline POS machine API action target for order
     */
    public static String ACTION_API_ORDER = apiHostURL_POS + "payment/pay/order";
    /**
     * offline POS machine API action target for query
     */
    public static String ACTION_API_QUERY_ORDER = apiHostURL_POS + "payment/pay/queryOrder";
    /**
     * offline POS machine API action target for refund
     */
    public static String ACTION_API_REVOKE = apiHostURL_POS + "payment/pay/revoke";
    /**
     * offline POS machine API action target for cancel
     */
    public static String ACTION_API_CANCEL = apiHostURL_POS + "payment/pay/cancel";    
    
	/**
	 * online scan mobile
	 */
    public static String ORDER_PAY = apiHostURL + "onlinePayment/v1_1/pay/orderPay";
	/**
	 * online mobile scan
	 */
    public static String PRE_PAY = apiHostURL + "onlinePayment/v1_1/pay/prePay";	
	/**
	 * online H5 payment
	 */
    public static String H5_PAY = apiHostURL + "onlinePayment/v1_1/pay/wapPay";
	/**
	 * online Get Pay URL
	 */
    public static String GET_PAY_URL = apiHostURL + "onlinePayment/v1_1/pay/getPayUrl";
	
	/**
	 * online query
	 */
    public static String ORDER_QUERY = apiHostURL + "onlinePayment/v1_1/pay/orderQuery";
	/**
	 * online refund
	 */
    public static String ORDER_REVOKE = apiHostURL + "onlinePayment/v1_1/pay/revoke";    
    
    
    private static String MID_ONLINE_SETTING = "100100010000025";
    private static String APPID_ONLINE_SETTING = "5005642018001";
    private static String APPSECURE_ONLINE_SETTING = "fdb811ec923f14f2a4c4f37435a3e451";

    static
    {
    	INSTANCE = new AppConfigInitUtil();
    }

    private AppConfigInitUtil()
    {
        initSettings();
        loadSettingsFromPropertiesFile();
    }
    
    private void loadSettingsFromPropertiesFile()
    {
    	try {
    		System.out.println("Trying to load mid from properties file.");
    		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    		InputStream input = classLoader.getResourceAsStream("application.properties");
    		Properties props = new Properties();
    		props.load(input);

    		String midOnlineInProps = props.getProperty("midOnline").trim();
    		String appIdOnlineInProps = props.getProperty("appIdOnline").trim();
    		String appSecretOnlineInProps = props.getProperty("appSecretOnline").trim();    
    		System.out.println("Loaded settings from properties file for mid:"+midOnlineInProps);
    		if(midOnlineInProps != null && midOnlineInProps.length() > 0 && 
    				appIdOnlineInProps != null && appIdOnlineInProps.length() > 0 &&
    						appSecretOnlineInProps != null && appSecretOnlineInProps.length() > 0) {
    			MID_ONLINE_SETTING = midOnlineInProps;
    			APPID_ONLINE_SETTING = appIdOnlineInProps;
    			APPSECURE_ONLINE_SETTING = appSecretOnlineInProps;
    	        System.out.println("Set the mid by properties file:"+midOnlineInProps);
    		}
    	}
    	catch(Exception e) {
    		System.out.println("Failed to load settings from properties file.");
    		e.printStackTrace();
    	}
    }


    public final String getAppIdOnline()
    {
        return appIdOnline;
    }


    public final String getAppIdUnderline()
    {
        return appIdUnderline;
    }


    public final String getAppSecretOnline()
    {
        return appSecretOnline;
    }


    public final String getAppSecretUnderline()
    {
        return appSecretUnderline;
    }


    public final String getMidOnline()
    {
        return midOnline;
    }


    public final String getMidUnderline()
    {
        return midUnderline;
    }
    
    public final String getAppIdByMid(String mid)
    {
    	String appId = appIdOnline;
    	if(mid != null && mid.compareTo(midUnderline) == 0) {
    		appId = appIdUnderline;
    	}
    	return appId;
    }
    
    public final String getAppSecretByMid(String mid)
    {
    	String appSecret = appSecretOnline;
    	if(mid != null && mid.compareTo(midUnderline) == 0) {
    		appSecret = appSecretUnderline;
    	}
    	return appSecret;
    }
    
    public final String getTomcatFolder()
    {
    	return tomcatFolder;
    }
    
    public final String getTomcatWebAppImageFolder()
    {
    	return tomcatWebAppImageFolder;
    }

    public final String getDefaultReturnURL()
    {
    	return defaultReturnURL;
    }
    
    
    public void setMidInfoForH5() 
    {
    	// Merchant ID: 100100010000009
    	// Appid: 5005642017007
    	// AppSecret: 052366873962708184168aaa57a32295
        midOnline = "100100010000026"; 
        appIdOnline = "5005642018002";
        appSecretOnline = "72f58bc3ee89af2fd4b4396d007c4f76";
    }
    
    public void setMidInfoForOnlineAPI() 
    {
    	// Merchant ID: 100105100000011
    	// Appid: 5005642017006
    	// AppSecret: 2ce9f51261e21ba6a087ee239160f0b1    	
    	
    	midOnline = MID_ONLINE_SETTING;
    	appIdOnline = APPID_ONLINE_SETTING;
    	appSecretOnline = APPSECURE_ONLINE_SETTING;    	
    	
    	// midOnline = "100100010000025";
        // appIdOnline = "5005642018001";
        // appSecretOnline = "fdb811ec923f14f2a4c4f37435a3e451";  
    }
    
    public void setMidInfoForOffline()
    {
    	// Merchant ID: 100100010000010
    	// Appid: 5005642017008
    	// AppSecret: 9f038a244e7fccdfe448221a1ce1c0cd
    	// live merchant id is Good
        midUnderline = "100100010000040";
        appIdUnderline = "5005642018017";
        appSecretUnderline = "380d53917b0d8a3fe33cfb7dd3e6c387";
    }
    
    private void setTomcatDefaultFolder() 
    {
    	String catalina_home_env = System.getenv("CATALINA_HOME");
    	String catalina_home = "/home/tomcat/tomcat7";
    	if(catalina_home_env != null && catalina_home_env.length() > 0) {
    		catalina_home = catalina_home_env;
    	}
    	setTomcatFolder(catalina_home + "/");
    	setTomcatWebAppImageFolder(catalina_home + "/webapps/" + tomcatWebAppName + "/images/");
    	setDefaultReturnURL("https://demo.motionpay.org/" + tomcatWebAppName + "/CallbackServlet");
    }

    private void setTomcatFolder(String str) 
    {
    	tomcatFolder = str;
    }

    public String getTomcatWebAppName()
    {
    	return tomcatWebAppName;
    }
    
    public void setTomcatWebAppName(String str)
    {
    	tomcatWebAppName = str;
    	setTomcatDefaultFolder();
    }
    
    public void setTomcatWebAppImageFolder(String str) 
    {
    	tomcatWebAppImageFolder  = str;
    }
    
    public void setDefaultReturnURL(String str)
    {
    	defaultReturnURL = str;
    }

    public void setReturnHostURL(String str)
    {
    	returnHostURL = str;
    	setDefaultReturnURL(returnHostURL + tomcatWebAppName + "/CallbackServlet");
    }

    public void initSettings()
    {
    	setMidInfoForH5();
    	setMidInfoForOnlineAPI();
    	setMidInfoForOffline();
    	setTomcatDefaultFolder();
    }

}