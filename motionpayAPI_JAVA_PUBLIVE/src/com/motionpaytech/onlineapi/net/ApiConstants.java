
package com.motionpaytech.onlineapi.net;

/**
 * TODO
 * Created by Song on 2017/11/1.
 */
public final class ApiConstants
{

    public static String ACTION_API_CANCEL = "/pay/cancel";

    public static String ACTION_API_ONLINE_CANCEL;

    public static String ACTION_API_ONLINE_GET_PAY_URL;

    public static String ACTION_API_ONLINE_ORDER_PAY;

    public static String ACTION_API_ONLINE_PREPAY;

    public static String ACTION_API_ONLINE_QUERY_ORDER;

    public static String ACTION_API_ONLINE_REVOKE;

    public static String ACTION_API_ONLINE_WAP_PAY;

    public static String ACTION_API_ORDER = "/pay/order";

    public static String ACTION_API_QUERY_ORDER = "/pay/queryOrder";

    public static String ACTION_API_REVOKE = "/pay/revoke";
    public static ApiConstants INSTANCE;
    
    static {
        new ApiConstants();
    }
    
    private ApiConstants() {
        INSTANCE = this;
        ACTION_API_ORDER = "/pay/order";
        ACTION_API_QUERY_ORDER = "/pay/queryOrder";
        ACTION_API_REVOKE = "/pay/revoke";
        ACTION_API_CANCEL = "/pay/cancel";
        ApiConstants.ACTION_API_ONLINE_ORDER_PAY = "/pay/orderPay";
        ApiConstants.ACTION_API_ONLINE_PREPAY = "/pay/prePay";
        ApiConstants.ACTION_API_ONLINE_QUERY_ORDER = "/pay/orderQuery";
        ApiConstants.ACTION_API_ONLINE_REVOKE = "/pay/revoke";
        ApiConstants.ACTION_API_ONLINE_CANCEL = "/pay/cancel";
        ApiConstants.ACTION_API_ONLINE_WAP_PAY = "/pay/wapPay";
        ApiConstants.ACTION_API_ONLINE_GET_PAY_URL = "/pay/getPayUrl";
    }
    
 
    public final String getACTION_API_CANCEL() {
        return ApiConstants.ACTION_API_CANCEL;
    }
    
 
    public final String getACTION_API_ONLINE_CANCEL() {
        return ApiConstants.ACTION_API_ONLINE_CANCEL;
    }
    
 
    public final String getACTION_API_ONLINE_GET_PAY_URL() {
        return ApiConstants.ACTION_API_ONLINE_GET_PAY_URL;
    }
    
 
    public final String getACTION_API_ONLINE_ORDER_PAY() {
        return ApiConstants.ACTION_API_ONLINE_ORDER_PAY;
    }
    
 
    public final String getACTION_API_ONLINE_PREPAY() {
        return ApiConstants.ACTION_API_ONLINE_PREPAY;
    }
    
 
    public final String getACTION_API_ONLINE_QUERY_ORDER() {
        return ApiConstants.ACTION_API_ONLINE_QUERY_ORDER;
    }
    
 
    public final String getACTION_API_ONLINE_REVOKE() {
        return ApiConstants.ACTION_API_ONLINE_REVOKE;
    }
    
 
    public final String getACTION_API_ONLINE_WAP_PAY() {
        return ApiConstants.ACTION_API_ONLINE_WAP_PAY;
    }
    
 
    public final String getACTION_API_ORDER() {
        return ApiConstants.ACTION_API_ORDER;
    }
    
 
    public final String getACTION_API_QUERY_ORDER() {
        return ApiConstants.ACTION_API_QUERY_ORDER;
    }
    
 
    public final String getACTION_API_REVOKE() {
        return ApiConstants.ACTION_API_REVOKE;
    }
    
    public final void setACTION_API_ONLINE_CANCEL(String action_API_ONLINE_CANCEL) {       
        ApiConstants.ACTION_API_ONLINE_CANCEL = action_API_ONLINE_CANCEL;
    }
    
    public final void setACTION_API_ONLINE_GET_PAY_URL(String action_API_ONLINE_GET_PAY_URL) {
        ApiConstants.ACTION_API_ONLINE_GET_PAY_URL = action_API_ONLINE_GET_PAY_URL;
    }
    
    public final void setACTION_API_ONLINE_ORDER_PAY(String action_API_ONLINE_ORDER_PAY) {
        ApiConstants.ACTION_API_ONLINE_ORDER_PAY = action_API_ONLINE_ORDER_PAY;
    }
    
    public final void setACTION_API_ONLINE_PREPAY(String action_API_ONLINE_PREPAY) {
        ApiConstants.ACTION_API_ONLINE_PREPAY = action_API_ONLINE_PREPAY;
    }
    
    public final void setACTION_API_ONLINE_QUERY_ORDER(String action_API_ONLINE_QUERY_ORDER) {
        ApiConstants.ACTION_API_ONLINE_QUERY_ORDER = action_API_ONLINE_QUERY_ORDER;
    }
    
    public final void setACTION_API_ONLINE_REVOKE(String action_API_ONLINE_REVOKE) {
        ApiConstants.ACTION_API_ONLINE_REVOKE = action_API_ONLINE_REVOKE;
    }
    
    public final void setACTION_API_ONLINE_WAP_PAY(String action_API_ONLINE_WAP_PAY) {
        ApiConstants.ACTION_API_ONLINE_WAP_PAY = action_API_ONLINE_WAP_PAY;
    }
}