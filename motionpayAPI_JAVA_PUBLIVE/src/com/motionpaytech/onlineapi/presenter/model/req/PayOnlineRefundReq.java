
// 

package com.motionpaytech.onlineapi.presenter.model.req;

import com.motionpaytech.onlineapi.net.ApiConstants;



public final class PayOnlineRefundReq extends BaseOnlineReq
{

    private String out_trade_no;
    private int refund_amount;
    private int total_fee;
    
    public PayOnlineRefundReq() {
        this.out_trade_no = "";
        this.setAction(ApiConstants.INSTANCE.getACTION_API_ONLINE_REVOKE());
    }
    

    public final String getOut_trade_no() {
        return this.out_trade_no;
    }
    
    public final int getRefund_amount() {
        return this.refund_amount;
    }
    
    public final int getTotal_fee() {
        return this.total_fee;
    }
    
    public final void setOut_trade_no(final String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }
    
    public final void setRefund_amount(final int refund_amount) {
        this.refund_amount = refund_amount;
    }
    
    public final void setTotal_fee(final int total_fee) {
        this.total_fee = total_fee;
    }
}
