
// 

package com.motionpaytech.onlineapi.presenter.model.req;

import com.motionpaytech.onlineapi.net.ApiConstants;



public final class PayOnlineRevokeReq extends BaseOnlineReq
{

    private String out_trade_no;
    
    public PayOnlineRevokeReq() {
        this.out_trade_no = "";
        this.setAction(ApiConstants.INSTANCE.getACTION_API_ONLINE_CANCEL());
    }
    

    public final String getOut_trade_no() {
        return this.out_trade_no;
    }
    
    public final void setOut_trade_no(final String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }
}
