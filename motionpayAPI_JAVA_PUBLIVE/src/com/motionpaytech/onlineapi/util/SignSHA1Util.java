package com.motionpaytech.onlineapi.util;


import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Map.Entry;

import com.motionpaytech.fastjson.JSONObject;

import java.util.Set;
import java.util.TreeMap;


public final class SignSHA1Util
{
  public static SignSHA1Util INSTANCE = null;
  
  static
  {
    new SignSHA1Util();
  }
  
  private SignSHA1Util()
  {
    INSTANCE = (SignSHA1Util)this;
  }
  
  public static final String getSignOnline(final JSONObject jsonObject,final String s,final String s2) {
    {
      final Map<String, Object> innerMap = jsonObject.getInnerMap();
      /*
      Map<String, Object> innerMap = new HashMap<String, Object>();
      System.out.println("jsonObject before innerMap str is:"+jsonObject.toJSONString());
      jsonObject.putAll(innerMap);     
      System.out.println("innerMap size after jsonObject is:"+innerMap.size()); */

      String signature = "";

      String string = "";
      innerMap.remove("sign");
      final Map<String, Object> sortMapByKey = SignSHA1Util.INSTANCE.sortMapByKey(innerMap);
      if(innerMap.size() > 0) {
	      for (final Map.Entry<String, Object> entry : sortMapByKey.entrySet()) {
	        final String s3 = entry.getKey();
	        final Object value = entry.getValue();
	        final StringBuilder sb = new StringBuilder();
	
	        final String lowerCase = s3.toLowerCase();
	         string += sb.append(lowerCase).append("=").append(value).append("&").toString();
	      }
      }
      String string2 = string;
      if (s != null && s.length() > 0) {
        string2 = string;
        if (s2 != null && s2.length() > 0) {
          string2 = string + "appid=" + s + "&appsecret=" + s2;
        }
      }
      System.out.println("before sign:" + (String) string2);
      try {
        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
        crypt.reset();
        crypt.update(string2.getBytes(Charset.forName("UTF-8")));
        signature = byteToHex(crypt.digest()).toUpperCase();
      } catch (NoSuchAlgorithmException ex) {
        ex.printStackTrace();
      }
      final String upperCase = signature.toUpperCase();
      return upperCase;
    }
  }
  
  public static final String getSignUnderline(final JSONObject jsonObject,String paramString1,String paramString2) throws Exception
  {
	  return getSignOnline(jsonObject, paramString1, paramString2);
  }
  
  public static final String byteToHex(byte[] paramArrayOfByte)
  {
    Formatter localFormatter = new Formatter();
    int i = 0;
    while (i < paramArrayOfByte.length)
    {
      localFormatter.format("%02x", new Object[] { Byte.valueOf(paramArrayOfByte[i]) });
      i += 1;
    }
    String result = localFormatter.toString();
    localFormatter.close();
    return result;
  }
  
  public static final Map<String, Object> sortMapByKey(Map<String, ? extends Object> paramMap)
  {
    if ((paramMap == null) || (paramMap.isEmpty())) {
      return null;
    }
    TreeMap localTreeMap = new TreeMap((Comparator)new MapKeyComparator());
    localTreeMap.putAll(paramMap);
    return (Map)localTreeMap;
  }
  
  public static final class MapKeyComparator
    implements Comparator<String>
  {
    public int compare(String paramString1,String paramString2)
    {
      return paramString1.compareTo(paramString2);
    }
  }
}

