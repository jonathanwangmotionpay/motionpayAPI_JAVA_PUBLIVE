package com.motionpaytech.onlineapi.util;

import java.text.SimpleDateFormat;
import java.util.Date;


public final class TokenGenerater
{
  public static TokenGenerater INSTANCE = null;
  
  static
  {
    new TokenGenerater();
  }
  
  private TokenGenerater()
  {
    INSTANCE = (TokenGenerater)this;
  }
  
  private final long getRandom()
  {
    return Math.round(Math.random() * 89999);
  }
  

  public final String newToken()
  {
    // return new SimpleDateFormat("yyyyMMdd").format(new Date()) + getRandom();
    return new SimpleDateFormat("yyyyMMdd").format(new Date());
  }
}

