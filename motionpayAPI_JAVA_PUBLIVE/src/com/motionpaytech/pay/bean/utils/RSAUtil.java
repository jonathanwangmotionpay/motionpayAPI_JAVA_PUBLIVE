﻿package com.motionpaytech.pay.bean.utils;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.Provider;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * 公钥加解密参考
 *
 * 
 */
public class RSAUtil {



	/**
	 * 算法名称
	 * */
	private static final String ALGORITHOM = "RSA";
	/** */
	/**
	 * RSA最大加密明文大小
	 */
	private static final int MAX_ENCRYPT_BLOCK = 117;

	/** */
	/**
	 * RSA最大解密密文大小
	 */
	private static final int MAX_DECRYPT_BLOCK = 128;
	/**
	 * 默认的安全服务提供者
	 * */
	private static final Provider DEFAULT_PROVIDER = new BouncyCastleProvider();

	public static void main(String[] args) throws Exception
	{	
	}

	/*****************************************
	 * Description：使用给定的公钥加密给定的字符串<br/>
	 * 
	 * @param publicKey
	 *            给定的公钥。
	 * @param plaintext
	 *            字符串。
	 * @return 给定字符串的密文。
	 *******************************************/
	public static String encryptString(PublicKey publicKey, String plaintext)
	{
		if (publicKey == null || plaintext == null)
		{
			return null;
		}
		try
		{
			byte[] data = plaintext.getBytes("utf-8");

			byte[] en_data = encrypt(publicKey, data);
			return new String(Hex.encodeHex(en_data));
		} catch (Exception ex)
		{
		}
		return null;
	}

	/*****************************************
	 * Description：使用指定的公钥加密数据<br/>
	 * 
	 * @param publicKey
	 *            给定的公钥。
	 * @param data
	 *            要加密的数据。
	 * @return 加密后的数据。
	 *******************************************/
	public static byte[] encrypt(PublicKey publicKey, byte[] data)
			throws Exception
	{
		Cipher ci = Cipher.getInstance(ALGORITHOM, DEFAULT_PROVIDER);
		ci.init(Cipher.ENCRYPT_MODE, publicKey);

		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段加密
		while (inputLen - offSet > 0)
		{
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK)
			{
				cache = ci.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else
			{
				cache = ci.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();

		return encryptedData;
	}

	/**
	 * 指定的公钥解密
	 * 
	 * @param publicKey
	 * @param encrypttext
	 * @return
	 */
	public static String decryptStringByPublicKey(PublicKey publicKey,
			String encrypttext)
	{
		if (publicKey == null || StringUtils.isBlank(encrypttext))
		{
			return null;
		}
		try
		{
			byte[] en_data = Hex.decodeHex(encrypttext.toCharArray());
			byte[] data = decryptByPublicKey(publicKey, en_data);
			return new String(data);
		} catch (Exception ex)
		{
		}
		return null;
	}

	public static byte[] decryptByPublicKey(PublicKey publicKey, byte[] data)
			throws Exception
	{
		Cipher ci = Cipher.getInstance(ALGORITHOM, DEFAULT_PROVIDER);
		ci.init(Cipher.DECRYPT_MODE, publicKey);

		int inputLen = data.length;

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段解密
		while (inputLen - offSet > 0)
		{
			if (inputLen - offSet > MAX_DECRYPT_BLOCK)
			{
				cache = ci.doFinal(data, offSet, MAX_DECRYPT_BLOCK);
			} else
			{
				cache = ci.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();

		return decryptedData;
	}
}
